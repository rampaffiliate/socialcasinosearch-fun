export const ROUTES = {
  home: '/',
  search: '/search',
  about: '/about',
  contact: '/contact',
  terms: '/terms',
  privacy: '/privacy',
  notFount: '/404',
};
